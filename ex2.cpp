//cpp.utils

#include "Header.h"
#include "util.h"
#include <iostream>

void reverse(int* nums, unsigned int size) {

	stack* top;
	initStack(&top);
	int* arr = new int[size];
	int num;

	for (int i = 0; i < size; i++) {

		push(&top, nums[i]);
	}

	for (int i = 0; i < size; i++) {

		num = pop(&top);
		arr[i] = num;
		nums[i] = num;
	}

	//*nums = *arr;

}

int* reverse10() {

	int size = 10;
	int* arr = new int[size];

	for (int i = 0; i < 10; i++){

		std::cin >> arr[i];
	}

	reverse(arr, size);

	return arr;

}



//cpp.linkedList

#include "Header.h"
#include <iostream>

void initStack(stack** s) {

	*s = new stack();
}

void push(stack** s, unsigned int element) {

	// create new node temp and allocate memory 
	stack* temp;
	temp = new stack();

	// check if stack (heap) is full. Then inserting an element would 
	// lead to stack overflow 
	if (!temp) {
		printf("\nHeap Overflow\n");
		exit(1);
	}

	// initialize data into temp data field 
	temp->data = element;

	// put top pointer reference into temp link 
	temp->next = *s;

	// make temp as top of Stack 
	*s = temp;

}

int pop(stack** s) {

	if (s == NULL) {
		printf("\nStack Underflow\n");
		return -1;
	}

	stack* curr = *s;
	stack* temp = NULL;
	int data;

	temp = curr->next;
	data = curr->data;
	free(*s);
	*s = temp;

	return data;


}

void cleanStack(stack* s) {

	free(s);

}

// Function to print all the  
// elements of the stack  
void display(stack** s)
{
	struct stack* temp;

	// check for stack underflow 
	if (s == NULL) {
		printf("\nStack Underflow\n");
		exit(1);
	}
	else {
		temp = *s;
		while (temp->next != NULL) {

			// print node data
			printf("\n%d\n", temp->data);

			// assign temp link to temp 
			temp = temp->next;
		}
	}
}